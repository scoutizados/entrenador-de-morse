using UnityEngine;
using UnityEngine.UI;

public class TimerBar : MonoBehaviour
{
    public delegate void EndTimeCallback();
    private float _currenTime;
    private bool _finished = false;

    public Image bar;
    public Image border;
    public float seconds;
    public EndTimeCallback endTimeCallback;

    private void Start()
    {
        InitializeTimer();
    }

    public void InitializeTimer()
    {
        _currenTime = seconds;
    }

    private void Update()
    {
        if (_finished) return;

        _currenTime -= Time.deltaTime;
        bar.fillAmount = _currenTime / seconds;

        if(_currenTime <= 0)
        {
            _finished = true;
            if(endTimeCallback != null ) endTimeCallback();
            enabled = false;
        }
    }
}
