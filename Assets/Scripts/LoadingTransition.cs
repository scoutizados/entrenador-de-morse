using UnityEngine;
using UnityEngine.UI;

public class LoadingTransition : MonoBehaviour
{
    public GameObject splashScreen;
    public Image leftCourtain;
    public Image rightCourtain;
    public Image logo;
    public float transitionTime { get { return _courtainTime + _logoTime; } }

    private float _initialXPos = 400;
    private Vector3 _initialLogoRotation = new Vector3(0, 0, -45);
    private Vector3 _initialLogoScale = new Vector3(2, 2, 1);
    private float _courtainTime = 0.5f;
    private float _logoTime = 0.5f;
    
    private void Awake()
    {
        SetInitialPos();
    }

    private void SetInitialPos()
    {
        leftCourtain.rectTransform.anchoredPosition = new Vector2(-_initialXPos, 0);
        rightCourtain.rectTransform.anchoredPosition = new Vector2(_initialXPos, 0);
        logo.rectTransform.Rotate(_initialLogoRotation);
        logo.rectTransform.localScale = _initialLogoScale;
        logo.canvasRenderer.SetAlpha(0);
    }

    public void MakeTransitionIn()
    {
        LeanTween.moveLocalX(leftCourtain.gameObject, -_initialXPos, _courtainTime);
        LeanTween.moveLocalX(rightCourtain.gameObject, _initialXPos, _courtainTime).setOnComplete(AnimateLogoIn);
    }

    public void MakeTransitionOut()
    {
        logo.CrossFadeAlpha(0, _logoTime, false);
        LeanTween.scale(logo.gameObject, _initialLogoScale, _logoTime);
        LeanTween.rotate(logo.gameObject, _initialLogoRotation, _logoTime).setOnComplete(AnimateCourtainsOut);
    }

    private void AnimateLogoIn()
    {
        logo.CrossFadeAlpha(1, _logoTime, false);
        LeanTween.scale(logo.gameObject, Vector3.one, _logoTime);
        LeanTween.rotate(logo.gameObject, Vector3.zero, _logoTime);
    }

    private void AnimateCourtainsOut()
    {
        LeanTween.moveLocalX(leftCourtain.gameObject, -_initialXPos * 2, _courtainTime);
        LeanTween.moveLocalX(rightCourtain.gameObject, _initialXPos * 2, _courtainTime);
    }
}
