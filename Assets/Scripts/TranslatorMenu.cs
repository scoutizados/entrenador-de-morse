using UnityEngine;
using UnityEngine.UI;

public class TranslatorMenu : MonoBehaviour
{
    enum TranslationMode
    {
        toMorse,
        toLatin
    };

    private TranslationMode _translationMode = TranslationMode.toMorse;
    private MorseTranslator _translator;

    private string _latinText = "Texto en latino";
    private string _morseText = "Texto en morse";

    public Text inputTitle;
    public Text outputTitle;

    public Button playButton;

    public InputField inputField;
    public Text output;

    protected void Awake()
    {
        _translator = GameManager.instance.translator;
    }
    private void Start()
    {
        inputField.onValueChanged.AddListener(delegate { Translate(); });
    }
    

    public void Translate()
    {
        string translation;
        switch(_translationMode)
        {
            case TranslationMode.toMorse:
                translation = _translator.TranslateSentenceToMorse(inputField.text);
            break;

            case TranslationMode.toLatin:
                translation = _translator.TranslateSentenceToLatin(inputField.text);
                break;
            
            default:
                translation = "";
            break;
        }
        output.text = translation;
    }

    public void PlayMorse()
    {
        if (_translationMode != TranslationMode.toMorse) return;
        MorsePlayer.instance.PlayMessage(output.text);
    }

    public void StopMorse()
    {
        MorsePlayer.instance.StopMessage();
    }

    public void SwitchTranslationMode()
    {
        switch(_translationMode)
        {
            case TranslationMode.toMorse:
                _translationMode = TranslationMode.toLatin;
                inputTitle.text = _morseText;
                outputTitle.text = _latinText;
                playButton.interactable = false;
                break;

            case TranslationMode.toLatin:
                _translationMode = TranslationMode.toMorse;
                inputTitle.text = _latinText;
                outputTitle.text = _morseText;
                playButton.interactable = true;
                break;
        }

        var inputTemp = inputField.text;
        inputField.text = output.text;
        output.text = inputTemp;
    }
}
