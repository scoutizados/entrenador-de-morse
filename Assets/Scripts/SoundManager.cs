using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private float _defaultVolume = 0.5f;
    private bool _isMuted = false;

    public bool isMuted { get { return _isMuted; } }

    public AudioClip[] music;
    public AudioClip[] sfx;

    private AudioSource _musicChannel;
    private AudioSource _sfxChannel;

    public const int MUSIC_ITTY_BITTY_8_BIT = 0;
    public const int MUSIC_HYPERFUN = 1;
    public const int MUSIC_JUMPIN_BOOGIE_WOOGIE = 2;


    public const int SFX_CORRECT = 0;
    public const int SFX_WRONG = 1;
    public const int SFX_DOT = 2;
    public const int SFX_DASH = 3;
    public const int SFX_YEAH = 4;
    public const int SFX_FAIL = 5;


    private void Awake()
    {
        _musicChannel = Camera.main.gameObject.AddComponent<AudioSource>();
        _musicChannel.volume = _defaultVolume;
        _musicChannel.loop = true;

        _sfxChannel = Camera.main.gameObject.AddComponent<AudioSource>();
        _sfxChannel.volume = _defaultVolume;
    }

    public void PlayMusic(int musicId)
    {
        _musicChannel.clip = music[musicId];
        _musicChannel.Play();
    }

    public void StopMusic()
    {
        _musicChannel.Stop();
    }

    public void PlaySound(int soundId)
    {
        _sfxChannel.clip = sfx[soundId];
        _sfxChannel.Play();
    }

    public void CleanSFXChannel()
    {
        _sfxChannel.clip = null;
    }

    public bool IsSFXChannelClean()
    {
        if (_sfxChannel.clip == null) return true;
        return false;
    }

    public float GetCurrentSoundLength()
    {
        return _sfxChannel.clip.length;
    }

    public void Mute()
    {
        _musicChannel.volume = 0;
        _sfxChannel.volume = 0;
        _isMuted = true;
    }

    public void Unmute()
    {
        _musicChannel.volume = _defaultVolume;
        _sfxChannel.volume = _defaultVolume;
        _isMuted = false;
    }

    public void StopSFX()
    {
        _sfxChannel.Stop();
    }
}
