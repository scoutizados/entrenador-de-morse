using UnityEngine;
using UnityEngine.UI;

public class Label : MonoBehaviour
{
    private Text[] buttonsText;

    public float time = 0.3f;
    public Image image;
    public Text text;
    public Image background;
    public Button[] buttons;

    private void Awake()
    {
        buttonsText = new Text[buttons.Length];
        for (int i = 0; i < buttons.Length; i++)
        {
            buttonsText[i] = buttons[i].GetComponentInChildren<Text>();
        }
    }

    public virtual void Show()
    {
        background.canvasRenderer.SetAlpha(0);
        image.canvasRenderer.SetAlpha(1);
        text.canvasRenderer.SetAlpha(1);

        gameObject.SetActive(true);

        background.CrossFadeAlpha(0.8f, time, false);
        image.transform.localScale = Vector3.zero;
        LeanTween.scale(image.gameObject, Vector3.one, time);
    }

    public virtual void Hide()
    {
        background.CrossFadeAlpha(0, time, false);
        image.CrossFadeAlpha(0, time, false);
        text.CrossFadeAlpha(0, time, false);
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].image.CrossFadeAlpha(0, time, false);
            buttonsText[i].CrossFadeAlpha(0, time, false);
        }
        Invoke("SetHintActiveFalse", time);
    }

    private void SetHintActiveFalse()
    {
        gameObject.SetActive(false);
    }
}
