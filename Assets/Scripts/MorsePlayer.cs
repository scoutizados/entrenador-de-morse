using System.Collections;
using UnityEngine;

public class MorsePlayer: MonoBehaviour
{
    private static MorsePlayer _instance;
    private SoundManager _soundManager;
    private IEnumerator _playMessageCoroutine;
    public static MorsePlayer instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance == null) _instance = this;
        else Debug.LogError("There are two or more MorsePlayer instances");

        _soundManager = GameManager.instance.soundManager;
    }


    public void PlayMessage(string message)
    {
        StopMessage();

        _playMessageCoroutine = PlayMessageCoroutine(message);
        StartCoroutine(_playMessageCoroutine);
    }


    public void StopMessage()
    {
        _soundManager.StopSFX();
        if (_playMessageCoroutine != null) StopCoroutine(_playMessageCoroutine);
    }


    private IEnumerator PlayMessageCoroutine(string message)
    {
        foreach (var character in message)
        {
            switch(character)
            {
                case '.':
                    _soundManager.PlaySound(SoundManager.SFX_DOT);
                    break;

                case '-':
                    _soundManager.PlaySound(SoundManager.SFX_DASH);
                    break;

                case '/':
                    _soundManager.CleanSFXChannel();
                    yield return new WaitForSeconds(0.4f);
                    break;
            }
            if(!_soundManager.IsSFXChannelClean())
                yield return new WaitForSeconds(_soundManager.GetCurrentSoundLength());

        }
    }
}
