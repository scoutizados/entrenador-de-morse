using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizFactory
{
    public QuizQuestion[] questions = new QuizQuestion[100];
    public QuizFactory()
    {
        string question;
        string correctAnswer;
        string incorrectAnswer1;
        string incorrectAnswer2;

        /*0: C�mo se dice en morse?  */

        question = "�C�mo se dice CASA en morse?";
        correctAnswer = "-.-./.-/.../.-";
        incorrectAnswer1 = "-.-./.-/--/.-";
        incorrectAnswer2 = "-.-./.-/.../..";

        questions[0] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice AMOR en morse?";
        correctAnswer = ".-/--/---/.-.";
        incorrectAnswer1 = ".-./---/--/.-";
        incorrectAnswer2 = ".-/.-./---/--";

        questions[1] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice MESA en morse?";
        correctAnswer = "--/./.../.-";
        incorrectAnswer1 = "--/---/.-./.";
        incorrectAnswer2 = "--/.-/.../.-";

        questions[2] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice NUDO en morse?";
        correctAnswer = "-./..-/-../---";
        incorrectAnswer1 = "-././--/---";
        incorrectAnswer2 = "-./..-/.-../---";

        questions[3] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice PAPA en morse?";
        correctAnswer = ".--./.-/.--./.-";
        incorrectAnswer1 = ".--./../.--./---";
        incorrectAnswer2 = ".--././.--./.";

        questions[4] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice MORA en morse?";
        correctAnswer = "--/---/.-./.-";
        incorrectAnswer1 = "--/---/.-./---";
        incorrectAnswer2 = ".-/--/---/.-.";

        questions[5] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice ROMA en morse?";
        correctAnswer = ".-./---/--/.-";
        incorrectAnswer1 = ".-./.-/--/---";
        incorrectAnswer2 = ".-././--/---";

        questions[6] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice B�HO en morse?";
        correctAnswer = "-.../..-/..../---";
        incorrectAnswer1 = "-.../.-/--.--/---";
        incorrectAnswer2 = "-.../.-/-/.";

        questions[7] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice FARO en morse?";
        correctAnswer = "..-./.-/.-./---";
        incorrectAnswer1 = "..-./../-./---";
        incorrectAnswer2 = "..-./.-/--/.-";

        questions[8] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice ELFO en morse?";
        correctAnswer = "./.-../..-./---";
        incorrectAnswer1 = "./.-../..-./.-";
        incorrectAnswer2 = "./.-../--/---";

        questions[9] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice DIOS en morse?";
        correctAnswer = "-../../---/...";
        incorrectAnswer1 = "-../../.-/...";
        incorrectAnswer2 = "-../../-.-./.";

        questions[10] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice COSA en morse?";
        correctAnswer = "-.-./---/.../.-";
        incorrectAnswer1 = "-.-./.-/.../---";
        incorrectAnswer2 = "-.-./.-/---/...";

        questions[11] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice DEDO en morse?";
        correctAnswer = "-.././-../---";
        incorrectAnswer1 = "-../..-/-../.-";
        incorrectAnswer2 = "-../.-/-../---";

        questions[12] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice CONO en morse?";
        correctAnswer = "-.-./---/-./---";
        incorrectAnswer1 = "-.-./---/--/---";
        incorrectAnswer2 = "-.-./---/--/.-";

        questions[13] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice GOTA en morse?";
        correctAnswer = "--./--/-/.-";
        incorrectAnswer1 = "--./.-/-/---";
        incorrectAnswer2 = "--/---/-/.-";

        questions[14] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice GATO en morse?";
        correctAnswer = "--./.-/-/---";
        incorrectAnswer1 = "--./---/-/.-";
        incorrectAnswer2 = ".--./.-/-/---";

        questions[15] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice GRIS en morse?";
        correctAnswer = "--./.-./../...";
        incorrectAnswer1 = "-../../.-/...";
        incorrectAnswer2 = "--/../.-/...";

        questions[16] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice GOLF en morse?";
        correctAnswer = "--./---/.-../..-.";
        incorrectAnswer1 = "--./---/.-../.-";
        incorrectAnswer2 = "--./---/--/.-";

        questions[17] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice GOMA en morse?";
        correctAnswer = "--./---/--/.-";
        incorrectAnswer1 = "--./---/-/.-";
        incorrectAnswer2 = "--./.-/-/---";

        questions[18] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se dice MANO en morse?";
        correctAnswer = "--/.-/-./---";
        incorrectAnswer1 = "--/---/-./---";
        incorrectAnswer2 = "--/.-/.-../---";

        questions[19] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        /*20: Qu� palabra es?  */

        question = "�Qu� palabra es?: --./.-./..-/.-";
        correctAnswer = "GRUA";
        incorrectAnswer1 = "GUR�";
        incorrectAnswer2 = "GOMA";

        questions[20] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: --./..-/.-../.-";
        correctAnswer = "GULA";
        incorrectAnswer1 = "GRUA";
        incorrectAnswer2 = "GOMA";

        questions[21] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: ..../../.---/.-";
        correctAnswer = "HIJA";
        incorrectAnswer1 = "HIJO";
        incorrectAnswer2 = "LIJA";

        questions[22] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: ..../---/.-../.-";
        correctAnswer = "HOLA";
        incorrectAnswer1 = "MOLA";
        incorrectAnswer2 = "HORA";

        questions[23] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: ..-./.-./../---";
        correctAnswer = "FR�O";
        incorrectAnswer1 = "FINO";
        incorrectAnswer2 = "FOSA";

        questions[24] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: ..-./---/-/---";
        correctAnswer = "FOTO";
        incorrectAnswer1 = "MOTO";
        incorrectAnswer2 = "LOTO";

        questions[25] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: --./.-/.../.-";
        correctAnswer = "GASA";
        incorrectAnswer1 = "MASA";
        incorrectAnswer2 = "GATA";

        questions[26] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: ..../../--../.-";
        correctAnswer = "HIZO";
        incorrectAnswer1 = "HIZA";
        incorrectAnswer2 = "HIJA";

        questions[27] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: -/.-././-.";
        correctAnswer = "TREN";
        incorrectAnswer1 = "TR�O";
        incorrectAnswer2 = "TIRO";

        questions[28] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/.-./.--./.-";
        correctAnswer = "ARPA";
        incorrectAnswer1 = "ARMA";
        incorrectAnswer2 = "ARCA";

        questions[29] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/.-./---/...";
        correctAnswer = "AROS";
        incorrectAnswer1 = "AMOS";
        incorrectAnswer2 = "ALAS";

        questions[30] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/-/..-/-.";
        correctAnswer = "AT�N";
        incorrectAnswer1 = "ASNO";
        incorrectAnswer2 = "ATEO";

        questions[31] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/.../-./---";
        correctAnswer = "ASNO";
        incorrectAnswer1 = "ALMA";
        incorrectAnswer2 = "AT�N";

        questions[32] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: --.--/---/--.--/---";
        correctAnswer = "�O�O";
        incorrectAnswer1 = "�O�A";
        incorrectAnswer2 = "�A�O";

        questions[33] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/--.--/---/...";
        correctAnswer = "A�OS";
        incorrectAnswer1 = "AMOS";
        incorrectAnswer2 = "AMOR";

        questions[34] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/.-../-/---";
        correctAnswer = "ALTO";
        incorrectAnswer1 = "ALTA";
        incorrectAnswer2 = "ALOE";

        questions[35] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/.-../--/.-";
        correctAnswer = "ALMA";
        incorrectAnswer1 = "ARMA";
        incorrectAnswer2 = "ALTA";

        questions[36] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: .-/../.-./.";
        correctAnswer = "AIRE";
        incorrectAnswer1 = "ALGA";
        incorrectAnswer2 = "AGUA";

        questions[37] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: -.-./.-/.-./.-";
        correctAnswer = "CARA";
        incorrectAnswer1 = "CAMA";
        incorrectAnswer2 = "CASA";

        questions[38] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�Qu� palabra es?: -.-./.-/..-./.";
        correctAnswer = "CAF�";
        incorrectAnswer1 = "CAZA";
        incorrectAnswer2 = "CEJA";

        questions[39] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        /*40: C�mo se escribe letra?  */
        question = "�C�mo se escribe en morse la quinta letra de CONSTITUCI�N?";
        correctAnswer = "-";
        incorrectAnswer1 = "...";
        incorrectAnswer2 = "..";

        questions[40] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de INDEPENDENCIA?";
        correctAnswer = ".--.";
        incorrectAnswer1 = ".";
        incorrectAnswer2 = "-.";

        questions[41] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de INVOCACI�N?";
        correctAnswer = "-.-.";
        incorrectAnswer1 = "---";
        incorrectAnswer2 = ".-";

        questions[42] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de CALEIDOSCOPIO?";
        correctAnswer = "..";
        incorrectAnswer1 = ".";
        incorrectAnswer2 = "-..";

        questions[43] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de EL�CTRICO?";
        correctAnswer = "-";
        incorrectAnswer1 = "-.-.";
        incorrectAnswer2 = ".-.";

        questions[44] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de MATERIAL?";
        correctAnswer = ".-.";
        incorrectAnswer1 = ".";
        incorrectAnswer2 = "..";

        questions[45] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra SCOUTIZADOS?";
        correctAnswer = "-";
        incorrectAnswer1 = "..-";
        incorrectAnswer2 = "..";

        questions[46] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de ESTUFA?";
        correctAnswer = "..-.";
        incorrectAnswer1 = "..-";
        incorrectAnswer2 = ".-";

        questions[47] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de TRILL�N?";
        correctAnswer = ".-..";
        incorrectAnswer1 = "---";
        incorrectAnswer2 = "..";

        questions[48] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de MAFEKING?";
        correctAnswer = "-.-";
        incorrectAnswer1 = ".";
        incorrectAnswer2 = "..";

        questions[49] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de MANGRULLO?";
        correctAnswer = "--.";
        incorrectAnswer1 = ".-.";
        incorrectAnswer2 = "-.";

        questions[50] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de SOGU�N?";
        correctAnswer = "..-";
        incorrectAnswer1 = "--.";
        incorrectAnswer2 = "..";

        questions[51] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de CRIPTOGRAF�A?";
        correctAnswer = ".--.";
        incorrectAnswer1 = "..";
        incorrectAnswer2 = "-";

        questions[52] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de ORIENTACI�N?";
        correctAnswer = ".";
        incorrectAnswer1 = "..";
        incorrectAnswer2 = "-.";

        questions[53] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de MOCHILA?";
        correctAnswer = "....";
        incorrectAnswer1 = "-.-.";
        incorrectAnswer2 = "..";

        questions[54] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de CARPA?";
        correctAnswer = ".--.";
        incorrectAnswer1 = ".-.";
        incorrectAnswer2 = ".-";

        questions[55] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de BALLESTRINQUE?";
        correctAnswer = ".-..";
        incorrectAnswer1 = ".";
        incorrectAnswer2 = "...";

        questions[56] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de ESCOTA?";
        correctAnswer = "---";
        incorrectAnswer1 = "-.-.";
        incorrectAnswer2 = "---";

        questions[57] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de BOMBERO?";
        correctAnswer = "-...";
        incorrectAnswer1 = "--";
        incorrectAnswer2 = ".";

        questions[58] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de MARGARITA?";
        correctAnswer = "--.";
        incorrectAnswer1 = ".-.";
        incorrectAnswer2 = ".-";

        questions[59] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        /*60: C�mo se escribe letra contando desde el final?  */

        question = "�C�mo se escribe en morse la quinta letra de CONSTITUCI�N contando desde el final?";
        correctAnswer = "..-";
        incorrectAnswer1 = "-";
        incorrectAnswer2 = "-.-.";

        questions[60] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de ALFABETOS contando desde el final?";
        correctAnswer = "-...";
        incorrectAnswer1 = ".-";
        incorrectAnswer2 = ".";

        questions[61] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de INDEPENDENCIA contando desde el final?";
        correctAnswer = ".";
        incorrectAnswer1 = "-..";
        incorrectAnswer2 = "-.";

        questions[62] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de ACTUACIONES contando desde el final?";
        correctAnswer = "..";
        incorrectAnswer1 = "-.-.";
        incorrectAnswer2 = "---";

        questions[63] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de DEMOSTRACI�N contando desde el final?";
        correctAnswer = ".-";
        incorrectAnswer1 = "-.-.";
        incorrectAnswer2 = ".-.";

        questions[64] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de NUM�RICO contando desde el final?";
        correctAnswer = ".";
        incorrectAnswer1 = "--";
        incorrectAnswer2 = ".-.";

        questions[65] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de RECONOCIMIENTO contando desde el final?";
        correctAnswer = "..";
        incorrectAnswer1 = "--";
        incorrectAnswer2 = ".";

        questions[66] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de ALTURA contando desde el final?";
        correctAnswer = ".-..";
        incorrectAnswer1 = ".-";
        incorrectAnswer2 = "-";

        questions[67] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de NI�OS contando desde el final?";
        correctAnswer = "-.";
        incorrectAnswer1 = "..";
        incorrectAnswer2 = "--.--";

        questions[68] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la quinta letra de MENOS contando desde el final?";
        correctAnswer = "--";
        incorrectAnswer1 = ".";
        incorrectAnswer2 = "-.";

        questions[69] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de DESTORNILLADOR contando desde el final?";
        correctAnswer = ".-";
        incorrectAnswer1 = ".-..";
        incorrectAnswer2 = "-..";

        questions[70] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de HACHAS contando desde el final?";
        correctAnswer = "-.-.";
        incorrectAnswer1 = ".-";
        incorrectAnswer2 = "....";

        questions[71] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de SOGAS contando desde el final?";
        correctAnswer = "---";
        incorrectAnswer1 = "...";
        incorrectAnswer2 = "--.";

        questions[72] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de ALTURA contando desde el final?";
        correctAnswer = ".-..";
        incorrectAnswer1 = ".-";
        incorrectAnswer2 = "-";

        questions[73] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de RINC�N contando desde el final?";
        correctAnswer = "-.";
        incorrectAnswer1 = "..";
        incorrectAnswer2 = "-.-.";

        questions[74] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de BANDER�N contando desde el final?";
        correctAnswer = ".";
        incorrectAnswer1 = "-..";
        incorrectAnswer2 = ".-.";

        questions[75] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de PATRULLA contando desde el final?";
        correctAnswer = ".-..";
        incorrectAnswer1 = ".-";
        incorrectAnswer2 = "-";

        questions[76] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de GU�AS contando desde el final?";
        correctAnswer = "..-";
        incorrectAnswer1 = "--.";
        incorrectAnswer2 = "..";

        questions[77] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de SEISENA contando desde el final?";
        correctAnswer = "...";
        incorrectAnswer1 = "..";
        incorrectAnswer2 = ".";

        questions[78] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "�C�mo se escribe en morse la cuarta letra de PA�UELO contando desde el final?";
        correctAnswer = "..-";
        incorrectAnswer1 = "--.--";
        incorrectAnswer2 = ".";

        questions[79] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        /*80: Cuentas  */

        question = "8+4";
        correctAnswer = "-../---/-.-./.";
        incorrectAnswer1 = "-/.-././-.-./.";
        incorrectAnswer2 = "---/-./-.-./.";

        questions[80] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "9+3";
        correctAnswer = "-../---/-.-./.";
        incorrectAnswer1 = "-/.-././-.-./.";
        incorrectAnswer2 = "---/-./-.-./.";

        questions[81] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "4x5";
        correctAnswer = "...-/./../-./-/.";
        incorrectAnswer1 = "-./..-/./...-/.";
        incorrectAnswer2 = "---/-.-./..../---";

        questions[82] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "7-2";
        correctAnswer = "-.-./../-./-.-./---";
        incorrectAnswer1 = "-/.-././...";
        incorrectAnswer2 = "..././../...";

        questions[83] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "8+4";
        correctAnswer = "-../---/-.-./.";
        incorrectAnswer1 = "-/.-././-.-./.";
        incorrectAnswer2 = "---/-./-.-./.";

        questions[84] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "1/2 + 1/2";
        correctAnswer = "..-/-./---";
        incorrectAnswer1 = "-.-././.-./---";
        incorrectAnswer2 = "-../---/...";

        questions[85] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "3x3";
        correctAnswer = "-./..-/./...-/.";
        incorrectAnswer1 = "..././../...";
        incorrectAnswer2 = "-.../.-/-./.-/-./.-";

        questions[86] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "10-7";
        correctAnswer = "-/.-././...";
        incorrectAnswer1 = "-../---/...";
        incorrectAnswer2 = "..-/-./---";

        questions[87] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "10/5";
        correctAnswer = "-../---/...";
        incorrectAnswer1 = "-.-./../-./-.-./---";
        incorrectAnswer2 = ".--./.-/.--./.-";

        questions[88] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "1.379.457 x 0";
        correctAnswer = "..-/-./---";
        incorrectAnswer1 = "-.-././.-./---";
        incorrectAnswer2 = ".--./.-/-/---";

        questions[89] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "15/5";
        correctAnswer = "-/.-././...";
        incorrectAnswer1 = "-../---/...";
        incorrectAnswer2 = "-.-./../-./-.-./---";

        questions[90] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "7+6";
        correctAnswer = "-/.-././-.-./.";
        incorrectAnswer1 = "--/---/-./--";
        incorrectAnswer2 = "-../---/-.-./.";

        questions[91] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "1-13x0";
        correctAnswer = "-.-././.-./---";
        incorrectAnswer1 = "..-/-./---";
        incorrectAnswer2 = "-./.-/.-./../--..";

        questions[92] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "7-5";
        correctAnswer = "-../---/...";
        incorrectAnswer1 = "-/.-././...";
        incorrectAnswer2 = "..-/-./---";

        questions[93] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "1+1";
        correctAnswer = "-../---/...";
        incorrectAnswer1 = "..-/-./---";
        incorrectAnswer2 = "-.-./../-./-.-./---";

        questions[94] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "10x10";
        correctAnswer = "-.-./.././-.";
        incorrectAnswer1 = "--/../.-..";
        incorrectAnswer2 = "-.././-../---";

        questions[95] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "5+5";
        correctAnswer = "-../.././--..";
        incorrectAnswer1 = "..-/-./---";
        incorrectAnswer2 = "-.-./.././-.";

        questions[96] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "12/2";
        correctAnswer = "..././../...";
        incorrectAnswer1 = "-/.-././...";
        incorrectAnswer2 = "--/.-/-./---";

        questions[97] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "10-1";
        correctAnswer = "-./..-/./...-/.";
        incorrectAnswer1 = "---/-.-./..../---";
        incorrectAnswer2 = "-.-././.-./---";

        questions[98] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);

        question = "100-100";
        correctAnswer = "-.-././.-./---";
        incorrectAnswer1 = "..-/-./---";
        incorrectAnswer2 = "./.-.././..-./.-/-./-/.";

        questions[99] = new QuizQuestion(question, correctAnswer, incorrectAnswer1, incorrectAnswer2);
    }

    public QuizQuestion[] GiveMeQuestions(int count)
    {
        if (count >= questions.Length)
        {
            Debug.LogError("You are asking more questions than I have. Max: " + questions.Length);
            return null;
        }

        var randomIndexes = new int[count];

        for (int i = 0; i < randomIndexes.Length; i++)
        {
            var rnd = Random.Range(0, questions.Length);

            while (ExistsNumberInArray(randomIndexes, rnd))
            {
                rnd = Random.Range(0, questions.Length);
            }

            randomIndexes[i] = rnd;
        }

        var randomQuestions = new QuizQuestion[count];

        for (int i = 0; i < randomQuestions.Length; i++)
        {
            randomQuestions[i] = questions[randomIndexes[i]];
        }

        return randomQuestions;
    }

    private bool ExistsNumberInArray(int[] array, int search)
    {
        foreach (var number in array)
        {
            if (number == search) return true;
        }
        return false;
    }
}
