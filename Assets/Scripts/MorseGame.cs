using UnityEngine;
using UnityEngine.UI;

public class MorseGame : MonoBehaviour
{
    protected MorseGameCard[] _cards;
    protected int _currentCardId;
    protected bool _gameFinished = false;

    public Vector2 initialCardAnchorsMin = new Vector2(0.5f, 0.5f);
    public Vector2 initialCardAnchorsMax = new Vector2(0.5f, 0.5f);
    public Vector2 initialCardPivot = new Vector2(0.5f, 0.5f);
    public Vector2 initialCardSize = new Vector2(800, 780);
    public Vector3 initialCardPosition = new Vector3(800, 0, 0);
    public Vector3 initialCardScale = Vector3.one;

    protected int _cardsCount = 15;

    public MorseTranslator translator;
    public SoundManager soundManager;
    public GameObject cardContainer;
    public GameObject cardPrefab;
    public Text currentCardText;
    public Sprite correctSprite;
    public Sprite incorrectSprite;
    public Label hint;
    public GameObject endGameScreen;
    public GameObject gameOverScreen;

    public MorseGameCard currentCard { get { return _cards[_currentCardId]; } }

    protected virtual void Awake()
    {
        if (GameManager.instance != null)
        {
            translator = GameManager.instance.translator;
            soundManager = GameManager.instance.soundManager;
            CreateCards(_cardsCount);
            _currentCardId = 0;
            SetFirstCardPosition(currentCard.rectTransform);
            SetCurrentCardText();
        }
    }


    public void CreateCards(int count)
    {
        _cards = new MorseGameCard[count];

        for (int i = 0; i < count; i++)
        {
            var card = Instantiate(cardPrefab);
            card.transform.SetParent(cardContainer.transform);

            _cards[i] = card.GetComponent<MorseGameCard>();

            SetCardTransform(_cards[i].rectTransform);
            SetCardValues(i);
        }
    }

    protected virtual void SetCardValues(int index)
    {
        var card = _cards[index];
        card.questionCharacter.text = translator.alphabet[Random.Range(0, translator.alphabet.Length)].latin.ToString();
        card.inputMorse.text = "";

    }

    private void SetCardTransform(RectTransform rt)
    {
        rt.anchorMin = initialCardAnchorsMin;
        rt.anchorMax = initialCardAnchorsMax;
        rt.pivot = initialCardPivot;
        rt.sizeDelta = initialCardSize;
        rt.anchoredPosition = initialCardPosition;
        rt.localScale = initialCardScale;

        if(initialCardAnchorsMin == Vector2.zero && initialCardAnchorsMax == Vector2.one)
        {
            rt.offsetMin = new Vector2(800,0);
            rt.offsetMax = new Vector2(800,0);
        }
    }

    private void SetFirstCardPosition(RectTransform rt)
    {
        rt.anchoredPosition = Vector3.zero;

        if (initialCardAnchorsMin == Vector2.zero && initialCardAnchorsMax == Vector2.one)
        {
            rt.offsetMax = Vector2.zero;
            rt.offsetMin = Vector2.zero;
        }
    }

    public void GiveFeedback(bool isCorrect)
    {
        var result = _cards[_currentCardId].result;

        if (isCorrect)
        {
            result.sprite = correctSprite;
            soundManager.PlaySound(SoundManager.SFX_CORRECT);
        }
        else
        {
            result.sprite = incorrectSprite;
            soundManager.PlaySound(SoundManager.SFX_WRONG);
        }
       
        result.transform.localPosition = _cards[_currentCardId].resultInitialPos;
        result.gameObject.SetActive(true);

        LeanTween.moveLocalY(result.gameObject, _cards[_currentCardId].resultFinalPos.y, 0.5f);

        result.canvasRenderer.SetAlpha(0);
        result.CrossFadeAlpha(1, 0.5f, false);
    }


    public virtual void NextCard()
    {
        if (FinishGameCondition())
            FinishGame();
        else
        {
            LeanTween.moveLocalX(_cards[_currentCardId + 1].gameObject, 0, 0.5f);
            LeanTween.moveLocalX(_cards[_currentCardId].gameObject, -800, 0.5f);
            _currentCardId++;
            SetCurrentCardText();
        }
    }

    protected virtual bool FinishGameCondition()
    {
        return (_currentCardId == _cards.Length - 1);
    }

    protected virtual void SetCurrentCardText()
    {
        if (currentCardText == null) return;
        currentCardText.text = _currentCardId + 1 + "/" + _cards.Length;
    }

    protected void FinishGame(bool failed = false)
    {
        GameObject screen = null;
        int soundId = 0;

        if(failed)
        {
            screen = gameOverScreen;
            soundId = SoundManager.SFX_FAIL;
        }
        else
        {
            screen = endGameScreen;
            soundId = SoundManager.SFX_YEAH;
        }

        _gameFinished = true;
        LeanTween.moveLocalX(screen, 0, 0.5f).setEaseInOutQuad();
        soundManager.PlaySound(soundId);
    }

    public void ShowHint()
    {
        var character = _cards[_currentCardId].questionCharacter.text;
        hint.text.text = character + "\n" + translator.GetWord(character[0]) + "\n" + translator.GetMorseCharacter(character[0]);
        hint.Show();
    }

    public void HideHint()
    {
        hint.Hide();
    }
}
