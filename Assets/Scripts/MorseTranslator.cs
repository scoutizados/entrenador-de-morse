using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

public class MorseTranslator
{
    private MorseCharacter[] _alphabet = new MorseCharacter[]
    {
        new MorseCharacter('A', ".-", "ASNO"),
        new MorseCharacter('B', "-...", "BOBADILLA"),
        new MorseCharacter('C', "-.-.", "COCA-COLA"),
        new MorseCharacter('D', "-..", "DOLARES"),
        new MorseCharacter('E', ".", "EN"),
        new MorseCharacter('F', "..-.", "FLECHADORA"),
        new MorseCharacter('G', "--.", "GOLOSA"),
        new MorseCharacter('H', "....", "HIMALAYA"),
        new MorseCharacter('I', "..", "INDIA"),
        new MorseCharacter('J', ".---", "JABONOSO"),
        new MorseCharacter('K', "-.-", "KOH-I-NOOR"),
        new MorseCharacter('L', ".-..", "LIMONADA"),
        new MorseCharacter('M', "--", "MONO"),
        new MorseCharacter('N', "-.", "NOCHE"),
        new MorseCharacter('�', "--.--", "�O�O PECOSO"),
        new MorseCharacter('O', "---", "OZONO"),
        new MorseCharacter('P', ".--.", "PISOTONES"),
        new MorseCharacter('Q', "--.-", "QOCO LISO"),
        new MorseCharacter('R', ".-.", "RAMONA"),
        new MorseCharacter('S', "...", "SETENTA"),
        new MorseCharacter('T', "-", "TOS"),
        new MorseCharacter('U', "..-", "UNICO"),
        new MorseCharacter('V', "...-", "VENTILADOR"),
        new MorseCharacter('W', ".--", "WEBOLO"),
        new MorseCharacter('X', "-..-", "XOCHIMILCO"),
        new MorseCharacter('Y', "-.--", "YO TE SOPLO"),
        new MorseCharacter('Z', "--..", "ZORRO LIBRE"),
    };

    public MorseCharacter[] alphabet { get { return _alphabet; } }
    
    public string GetMorseCharacter(char latinCharacter)
    {
        latinCharacter = char.ToUpper(latinCharacter);
        
        foreach (var character in _alphabet)
        {
            if (character.latin == latinCharacter)
                return character.morse;
        }

        return null;
    }

    public char GetLatinCharacter(string morseCharacter)
    {
        foreach (var character in _alphabet)
        {
            if (character.morse == morseCharacter)
                return character.latin;
        }

        return '?';
    }

    public string GetWord(string morseCharacter)
    {
        foreach(var character in _alphabet)
        {
            if (character.morse == morseCharacter)
                return character.word;
        }

        return null;
    }

    public string GetWord(char latinCharacter)
    {
        foreach (var character in _alphabet)
        {
            if (character.latin == latinCharacter)
                return character.word;
        }

        return null;
    }

    public string TranslateWordToMorse(string word)
    {
        word = RemoveAccents(word);

        var morseWord = new StringBuilder();
        foreach (var character in word)
        {
            morseWord.Append(GetMorseCharacter(character));
            morseWord.Append('/');
        }

        return morseWord.ToString();
    }

    public string TranslateSentenceToMorse(string sentence)
    {
        var morseSentence = new StringBuilder();

        var words = sentence.Split(' ');

        foreach (var word in words)
        {
            morseSentence.Append(TranslateWordToMorse(word));
            morseSentence.Append('/');
        }

        return morseSentence.ToString();
    }

    public string TranslateWordToLatin(string word)
    {
        var characters = word.Split('/');
        var latinWord = new StringBuilder();

        foreach (var character in characters)
        {
            latinWord.Append(GetLatinCharacter(character));
        }

        return latinWord.ToString();
    }


    public string TranslateSentenceToLatin(string sentence)
    {
        var words = sentence.Split(new string[] { "//" }, System.StringSplitOptions.None);
        var latinSentence = new StringBuilder();
        foreach (var word in words)
        {
            latinSentence.Append(TranslateWordToLatin(word));
            latinSentence.Append(' ');
        }

        return latinSentence.ToString();
    }

    private string RemoveAccents(string inputString)
    {
        var result = string.Concat(Regex.Replace(inputString, @"(?i)[\p{L}-[�a-z]]+", m =>
                m.Value.Normalize(NormalizationForm.FormD)
            )
            .Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark));

        return result;
    }
}
