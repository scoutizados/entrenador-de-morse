using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizQuestion
{
    public string question;
    public string[] answers = new string[3];

    public const int CORRECT_ANSWER_INDEX = 0;

    public QuizQuestion(string question, string correctAnswer, string incorrectAnswer1, string incorrectAnswer2)
    {
        this.question = question;
        answers[0] = correctAnswer;
        answers[1] = incorrectAnswer1;
        answers[2] = incorrectAnswer2;
    }
}
