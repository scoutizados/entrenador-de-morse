using UnityEngine;
using UnityEngine.UI;

public class MorseGameCard : MonoBehaviour
{
    public delegate void PressButtonCallback(int id);
    
    public Text question;
    public Text questionCharacter;
    public Text inputMorse;
    public RectTransform rectTransform;
    public Image result;
    public Text[] answers;
    public PressButtonCallback pressButtonCallback;
    public TimerBar timerBar;

    public Vector3 resultInitialPos = Vector3.zero;
    public Vector3 resultFinalPos = new Vector3(0, -65, 0);

    public void PressButton(int id)
    {
        pressButtonCallback(id);
    }

    public void StartTimer()
    {
        timerBar.enabled = true;
        timerBar.InitializeTimer();
    }

    public void StopTimer()
    {
        timerBar.enabled = false;
        timerBar.bar.CrossFadeAlpha(0, 0.3f, false);
        timerBar.border.CrossFadeAlpha(0, 0.3f, false);
    }
}
