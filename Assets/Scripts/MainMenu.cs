using UnityEngine;
public class MainMenu : MonoBehaviour
{
    public GameObject toTrainMenu;
    public GameObject translatorMenu;
    public GameObject alphabetMenu;

    private GameObject _currentWindow;
    private Vector3 _hidePosition = new Vector3(800, 0, 0);
    private float _tweenTime = 0.5f;
    private void Awake()
    {
        toTrainMenu.transform.localPosition = _hidePosition;
        translatorMenu.transform.localPosition = _hidePosition;
        alphabetMenu.transform.localPosition = _hidePosition;
    }

    private void Start()
    {
        GameManager.instance.soundManager.StopMusic();
    }

    public void ShowToTrainMenu()
    {
        ShowMenu(toTrainMenu);
    }

    public void ShowTranslatorMenu()
    {
        ShowMenu(translatorMenu);
    }

    public void ShowAlphabetMenu()
    {
        ShowMenu(alphabetMenu);
    }

    private void ShowMenu(GameObject menu)
    {
        if (LeanTween.isTweening(menu)) return;
        menu.SetActive(true);
        LeanTween.moveLocalX(menu, 0, 0.5f).setEaseInOutQuad().setOnComplete(HideMainMenu);
        _currentWindow = menu;
    }

    public void HideMainMenu()
    {
        gameObject.SetActive(false);
    }

    public void ShowMainMenu()
    {
        if (LeanTween.isTweening(_currentWindow)) return;
        gameObject.SetActive(true);
        LeanTween.moveLocalX(_currentWindow, _hidePosition.x, _tweenTime).setEaseInOutQuad().setOnComplete(HideCurrentWindow);
    }

    public void HideCurrentWindow()
    {
        if (LeanTween.isTweening(_currentWindow)) return;
        _currentWindow.SetActive(false);
        _currentWindow = gameObject;
    }

    public void ShowTutorial()
    {
        Application.OpenURL("https://www.youtube.com/watch?v=dQRkcVWnNEc");
    }

    public void LoadNoobGame()
    {
        GameManager.instance.LoadNoobGame();
    }

    public void LoadQuizGame()
    {
        GameManager.instance.LoadQuizGame();
    }

    public void LoadTimeTrialGame()
    {
        GameManager.instance.LoadTimeTrialGame();
    }
}
