using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Header : MonoBehaviour
{
    private int _currentLifes;
    public int lifes = 3;

    public delegate void RunOutLifesCallback();
    public Sprite soundEnabledSprite;
    public Sprite soundDisabledSprite;

    public Image soundButtonImage;
    public Image heart;
    public Text lifesText;
    public RunOutLifesCallback runOutLifesCallback;
    public Color defaultColor;
    public Color warningColor;
    public Color dangerColor;

    private void Awake()
    {
        if(GameManager.instance != null)
        {
            var isMuted = GameManager.instance.soundManager.isMuted;
            SetSoundSprite(isMuted);
            SetHeartColor();
            _currentLifes = lifes;
            if(lifesText != null) lifesText.text = lifes.ToString();
        }
    }

    public bool SubstractLife()
    {
        if (_currentLifes == 1)
        {
            if(runOutLifesCallback != null)
            runOutLifesCallback();
            return true;
        }
        else
        {
            _currentLifes--;
            lifesText.text = _currentLifes.ToString();

            SetHeartColor();

            return false;
        }
    }

    private void SetHeartColor()
    {
        if (heart == null) return;

        switch (_currentLifes)
        {
            case 1:
                heart.color = dangerColor;
                break;

            case 2:
                heart.color = warningColor;
                break;

            default:
                heart.color = defaultColor;
                break;
        }
    }

    public void SwitchSound()
    {
        var isMuted = GameManager.instance.SwitchSound();
        SetSoundSprite(isMuted);
    }

    private void SetSoundSprite(bool isMuted)
    {
        if (isMuted)
            soundButtonImage.sprite = soundDisabledSprite;
        else
            soundButtonImage.sprite = soundEnabledSprite;
    }

    public void BackToMainMenu()
    {
        GameManager.instance.LoadMainMenu();
    }
}
