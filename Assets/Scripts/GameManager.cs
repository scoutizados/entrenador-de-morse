using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    
    public MorseTranslator translator = new MorseTranslator();
    public SoundManager soundManager;
    public MorsePlayer morsePlayer;
    public LoadingTransition loadingTransition;
    public static GameManager instance 
    { 
        get 
        { 
            if (_instance == null && Initializer.instance == null)
            {
                SceneManager.LoadScene(0);
                return null;
            }
            return _instance; 
        } 
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Debug.LogError("There are two or more GameManager instances");
    }

    public void LoadNoobGame()
    {
        StartCoroutine(LoadScene("NoobGame"));
    }

    public void LoadMainMenu()
    {
        StartCoroutine(LoadScene("MainMenu"));
    }

    public void LoadQuizGame()
    {
        StartCoroutine(LoadScene("QuizGame"));
    }

    public void LoadTimeTrialGame()
    {
        StartCoroutine(LoadScene("TimeTrialGame"));
    }

    private IEnumerator LoadScene(string name)
    {
        loadingTransition.MakeTransitionIn();
        yield return new WaitForSeconds (loadingTransition.transitionTime + 0.5f);
        SceneManager.LoadScene(name);
        loadingTransition.MakeTransitionOut();
    }

    public bool SwitchSound()
    {
        if (soundManager.isMuted)
            soundManager.Unmute();
        else
            soundManager.Mute();

        return soundManager.isMuted;
    }
}
