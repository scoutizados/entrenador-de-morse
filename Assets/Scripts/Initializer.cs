using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initializer : MonoBehaviour
{
    private static Initializer _instance;
    public static Initializer instance { get { return _instance; } }
    public GameObject gameManagerPrefab;
    private void Awake()
    {
        if (_instance == null) _instance = this;
        else
        {
            Debug.LogError("There are two or more Initializer instances");
            return;
        }
        if(GameManager.instance == null)
        {
            Instantiate(gameManagerPrefab);
        }
    }
}
