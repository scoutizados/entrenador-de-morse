using UnityEngine;
using UnityEngine.UI;

public class NoobCard : MonoBehaviour
{
    public Text questionCharacter;
    public Text inputMorse;
    public RectTransform rectTransform;
    public Image result;


    public Vector3 resultInitialPos = Vector3.zero;
    public Vector3 resultFinalPos = new Vector3(0, -65, 0);
}
