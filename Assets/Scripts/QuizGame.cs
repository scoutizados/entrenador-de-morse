using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizGame : MorseGame
{
    private QuizQuestion[] _questions;
    private int _currentCorrectAnswers = 0;

    public Header header;
    public int correctAnswersToWin = 15;

    protected override void Awake()
    {
        var questionsCount = correctAnswersToWin + header.lifes;
        _cardsCount = questionsCount;
        _questions = new QuizFactory().GiveMeQuestions(questionsCount);
        header.runOutLifesCallback = RunOutLifesCallback;
        base.Awake();   
    }

    private void Start()
    {
        soundManager.PlayMusic(SoundManager.MUSIC_HYPERFUN);
        currentCard.StartTimer();
    }

    protected override void SetCardValues(int index)
    {
        var card = _cards[index];
        var question = _questions[index];

        card.question.text = question.question;

        for (int i = 0; i < card.answers.Length; i++)
        {            
            card.answers[i].text = question.answers[i];
        }

        int rnd = Random.Range(0, card.answers.Length);
        var temp = card.answers[rnd].text;
        card.answers[rnd].text = question.answers[QuizQuestion.CORRECT_ANSWER_INDEX];
        card.answers[0].text = temp;

        card.pressButtonCallback = AnswerQuestion;
        card.timerBar.endTimeCallback = TimeFinishedEvent;
    }

    public void AnswerQuestion(int answerId)
    {
        currentCard.StopTimer();

        var answerIsCorrect = AnswerIsCorrect(answerId);
        

        GiveFeedback(answerIsCorrect);

        bool runOutOfLifes = false;

        if(!answerIsCorrect) runOutOfLifes = header.SubstractLife();
        
        if(!runOutOfLifes) Invoke("NextCard", 1f);
    }

    private void TimeFinishedEvent()
    {
        GiveFeedback(false);
        Invoke("NextCard", 1f);
    }

    private void RunOutLifesCallback()
    {
        FinishGame(true);
    }

    private bool AnswerIsCorrect(int answerId)
    {
        var userAnswer = currentCard.answers[answerId].text;
        var correctAnswer = _questions[_currentCardId].answers[QuizQuestion.CORRECT_ANSWER_INDEX];

        if (userAnswer == correctAnswer)
        {
            _currentCorrectAnswers++;
            return true;
        }

        return false;
    }

    public override void NextCard()
    {
        base.NextCard();
        if (!_gameFinished)
            currentCard.StartTimer();
    }

    protected override bool FinishGameCondition()
    {
        return (_currentCorrectAnswers == correctAnswersToWin);
    }

    protected override void SetCurrentCardText()
    {
        currentCardText.text = _currentCorrectAnswers + 1 + "/" + correctAnswersToWin;
    }
}
