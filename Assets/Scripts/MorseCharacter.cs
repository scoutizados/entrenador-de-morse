public class MorseCharacter
{
    public char latin;
    public string morse;
    public string word;

    public MorseCharacter(char latin, string morse, string word)
    {
        this.latin = latin;
        this.morse = morse;
        this.word = word;
    }
}
