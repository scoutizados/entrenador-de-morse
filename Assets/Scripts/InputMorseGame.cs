using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMorseGame : MonoBehaviour
{
    public MorseGame morseGame;
    public int maxCharacterCount = 5;
    public void WriteInput(string input)
    {
        if (morseGame.currentCard.inputMorse.text.Length >= maxCharacterCount) return;
        morseGame.currentCard.inputMorse.text += input;
        GameManager.instance.morsePlayer.PlayMessage(input);
    }

    public void DeleteCharacter()
    {
        if (morseGame.currentCard.inputMorse.text.Length == 0) return;
        var text = morseGame.currentCard.inputMorse.text;
        morseGame.currentCard.inputMorse.text = text.Remove(text.Length - 1);
    }

    public void SendInput()
    {
        if (CheckInputCorrect())
        {
            morseGame.GiveFeedback(true);
            Invoke("NextCard", 1f);
        }
        else
        {
            morseGame.GiveFeedback(false);
            Invoke("ResetInput", 1f);
        }
    }

    private void NextCard()
    {
        morseGame.NextCard();
    }

    private bool CheckInputCorrect()
    {
        var translation = morseGame.translator.TranslateWordToLatin(morseGame.currentCard.inputMorse.text);

        if (translation == morseGame.currentCard.questionCharacter.text)
            return true;

        return false;
    }


    private void ResetInput()
    {
        morseGame.currentCard.inputMorse.text = "";
        morseGame.currentCard.result.gameObject.SetActive(false);
    }
}
