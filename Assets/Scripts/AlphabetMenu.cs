using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class AlphabetMenu : MonoBehaviour
{
    public Text leftColumn;
    public Text rightColumn;

    private string _leftColumnTextMorse;
    private string _rightColumnTextMorse;
    private string _leftColumnTextLatin;
    private string _rightColumnTextLatin;

    private MorseTranslator _translator;

    protected void Awake()
    {
        _translator = GameManager.instance.translator;
        
        _leftColumnTextMorse = SetColumns('A', 'N', true);
        _rightColumnTextMorse = SetColumns('�', 'Z', true);

        _leftColumnTextLatin = SetColumns('A', 'N', false);
        _rightColumnTextLatin = SetColumns('�', 'Z', false);
    }

    private void Start()
    {
        WriteColumnsWords();
    }


    public void WriteColumnsMorse()
    {
        leftColumn.text = _leftColumnTextMorse;
        rightColumn.text = _rightColumnTextMorse;
    }

    public void WriteColumnsWords()
    {
        leftColumn.text = _leftColumnTextLatin;
        rightColumn.text = _rightColumnTextLatin;
    }

    private string SetColumns(char first, char last, bool morse)
    {
        var sbColumn = new StringBuilder();
        var started = false;

        foreach (var character in _translator.alphabet)
        {
            if (!started)
            {
                if (character.latin != first) continue;
                else started = true;
            }
            sbColumn.Append("<b>" + character.latin + "</b>");
            sbColumn.Append(' ');
            if (morse)
            {
                sbColumn.Append(character.morse);
            }
            else
            {
                sbColumn.Append(character.word);
            }
            sbColumn.Append('\n');

            if (character.latin == last)
                break;
        }

        return sbColumn.ToString();
    }
}
